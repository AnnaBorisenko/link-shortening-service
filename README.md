
## About this project

This service processes registered links, shortens them, sets a limit for transitions to a registered link, and allows you to set the "link lifetime".

When creating this project, the Laravel framework was used.

## Project Pages

The project consists of two pages:
- page for displaying registered links;
- page 404.

  
## page for displaying registered links

On the page for displaying registered links, you can:

- register a link;
- set a limit for visits to this link;
- set a "lifetime".

Registration of a new link occurs through a form in a modal window using an ajax request.

### Link options

All links have the following parameters:

- Transition limit - maximum
  the number of clicks on the link. 0 = unlimited;
- Link lifetime - set
  by user, but not more than 24 hours

Upon the expiration of the link's lifetime, or the exhaustion of the referral limit, during the transition
using a short link, the service redirects to a 404 page

## Notes

The shortened link by clicking on the link is redirected to the original
address.

The short link token is random, unique, consisting of numbers and letters (of different case), 8 characters long.

## From the author

Perhaps the work is not perfect, and there are flaws.
I would be glad to any criticism, because I understand that in this direction I still have a lot to learn and master.

# Все замечания принимаю 
- **на телеграмм**:  [@AnutkaBorisenko](https://t.me/AnutkaBorisenko).
- **на linkedin**:  [Anna Borisenko](https://www.linkedin.com/in/anna-borisenko-695837213/).


