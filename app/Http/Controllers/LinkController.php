<?php

namespace App\Http\Controllers;

use App\Http\Requests\LinkRequest;
use App\Models\Link;
use Illuminate\Support\Str;

class LinkController extends Controller
{
    public function index()
    {
        $links = Link::all();
        return view('home', compact('links'));
    }

    public function store(LinkRequest $request)
    {

        $datatocreate = $request->validated();
        $token = Str::random(8);
        $datatocreate['_token'] = $token;
        $datatocreate['code'] = Str::random(10);
        Link::create($datatocreate);
        return redirect()->route('links.index')->with('success', 'Link created successfully');
    }

    public function countedlinks($code)
    {
        $link = Link::where('code', $code)->first();
        $time = $link->created_at;
        $limit = $link->lifeTime;
        if($link->limit_count != 0){
            $link->count++;
            $link->save();
            if ($link->count > $link->limit_count || now()->diffInHours($time) >= $limit) {
                return view('404', compact('link'));
            }
        }
        return redirect($link->link);
    }

}
