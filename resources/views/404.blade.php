@extends('layouts.app')

@section('content')
    <div id="layoutError">
        <div id="layoutError_content">
            <main>
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-6">
                            <div class="text-center mt-4">
                                <img class="mb-4 img-error" src="{{ asset('img/error-404-monochrome.svg') }}"/>
                                @if($link->count > $link->limit_count)
                                    <p class="lead">Вы превысили число переходов по ссылке</p>
                                @else
                                    <p class="lead">"Время жизни ссылки истекло"</p>
                                @endif
                                <a href="{{ route('links.index') }}">
                                    Вернуться обратно
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </div>
    </div>
@endsection
