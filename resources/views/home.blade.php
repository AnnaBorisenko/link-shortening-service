@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="card bg-light-grey">
            <div class="card-header">
                <div class="row justify-content-between">
                    <h3 class="float-left">Список зарегистрированных ссылок</h3>
                    <!-- Button trigger modal -->
                    <button type="button" class="btn btn-outline-success right col-2" data-toggle="modal" data-target="#exampleModal">
                        Добавить ссылку
                    </button>
                    <!-- Modal -->
                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <form role="form" action="{{ route('links.store') }}" method="POST">
                                        @csrf
                                        <div class="card-body my-3 bg-light">

                                            <div class="form-group">
                                                <label for="link">Ссылка:</label>
                                                <input type="text" class="form-control" name="link" id="link" placeholder="введите ссылку" value="{{ old('link') }}">
                                                @error('link') <span class="text-danger error"><small>{{ $message }}</small></span>@enderror
                                            </div>
                                            <div class="form-group">
                                                <label for="limit_count">Дозволенное количество переходов по ссылке:</label>
                                                <input type="text" class="form-control" name="limit_count" id="limit_count" placeholder="укажите количество переходов по ссылке" value="{{ old('limit_count') }}">
                                                @error('limit_count') <span class="text-danger error"><small>{{ $message }}</small></span>@enderror
                                            </div>
                                            <div class="form-group">
                                                <label for="lifeTime">Время жизни ссылки в часах:</label>
                                                <input type="text" class="form-control" name="lifeTime" id="lifeTime" placeholder="введите время жизни ссылки" value="{{ old('lifeTime') }}">
                                                @error('lifeTime') <span class="text-danger error"><small>{{ $message }}</small></span>@enderror
                                            </div>
                                            <div class="form-group my-2">
                                                <button type="submit" class="btn btn-primary" id="submitForm">Submit</button>
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                        <!-- /.card-body -->

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{--    end modal--}}
                    {{--                    <a class="btn btn-outline-success right col-2" href="{{ route('create.create') }}">Добавить Автомобиль</a>--}}
                </div>
            </div>
            <div class="card-body" id="cardBody">
                <table id="myTable" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>##</th>
                        <th>Сокращенная ссылка</th>
                        <th>_token</th>
                        <th>Лимит переходов</th>
                        <th>Время жизни ссылки</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($links as $link)
                        <tr>
                            <td>{{ $link->id }}</td>
                            <td><a href="{{ route('links.countedlinks', $link->code) }}" target="_blank">{{ route('links.countedlinks', $link->code) }}</a></td>
                            <td>{{ $link->_token }}</td>
                            <td>{{ $link->limit_count == 0 ? 'безлимит' : $link->limit_count }}</td>
                            <td>{{ $link->lifeTime }}
                                @if($link->lifeTime == 1 || $link->lifeTime == 21)
                                    час
                                @elseif($link->lifeTime <= 4 ||$link->lifeTime>21)
                                    часа
                                @else
                                    часов
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function () {


            $(document).on('click', '#submitForm', function(){
                $.ajax({
                    url: "links",
                    type:"POST",
                    data:{
                        'link': $('#link').val(),
                        'limit_count': $('#limit_count').val(),
                        'lifeTime': $('#lifeTime').val(),
                        '_token': $('input[name=_token]').val(),
                    },
                    success:function(response) {
                        $('#cardBody').load(location.href + ' #cardBody')
                        $('#exampleModal').modal('hide');

                    },
                    error:function (response) {
                        $('#linkError').text(response.responseJSON.errors.link);
                        $('#limit_count').text(response.responseJSON.errors.limit_count);
                        $('#lifeTime').text(response.responseJSON.errors.lifeTime);
                    }
                });

            });
        });
    </script>
@endsection
