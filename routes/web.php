<?php

use App\Http\Controllers\LinkController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;


Route::get('/', [LinkController::class, 'index'])->name('links.index');
Route::post('/', [LinkController::class, 'store'])->name('links.store');
Route::get('/{code}', [LinkController::class, 'countedlinks'])->name('links.countedlinks');
Route::get('/error', [LinkController::class, 'error'])->name('links.error');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
